
//************************************************
// File:        MilesToKilometers
// Author:      Kevin Jones
// Date:        14 September 2017
// Course:      CPS 100
//
// PROBLEM STATEMENT: Write a program that converts miles to killometers. (one mile equals
// 1.6935 kilometers) Read the miles from the user as a floating value
//
//Inputs: number of miles (floating point number)
//Outputs: number of kilometers (floating point number)
// 
//************************************************
import java.util.Scanner;


public class MilesToKilometers
{

  public static void main(String[] args)
  {
    final double MILES_TO_KILOMETERS = 1.6935;
    Double miles = 0.0;
    Double killometers = 0.0;
    Scanner scan = new Scanner(System.in);
    miles = scan.nextDouble();
    scan.close();
    // calculate number of killometers
    killometers = MILES_TO_KILOMETERS * miles;

    // display number of kilometers
    System.out.println("you have travelled" + killometers + "killometers");

  }

}
