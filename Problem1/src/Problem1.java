
//************************************************
// File:        Problem1
// Author:      Kevin Jones
// Date:        23 September 2017
// Course:      CPS 100
//
// PROBLEM STATEMENT: Write a program that creates and prints a 
// random phone number
// of the  form XXX-XXX-XXXX. Include the dashes in the output.
// Don't let the first three digits be an 8 or 9 (but don't 
// be more restrictive than that), and make that the second set
// of three digits is not greater than 655. Hint: think through the easiest way
//to construct the phone number. Each digit does not determined separately.
// 
//
//Outputs: random telephone number.
// 
//************************************************
// import the utilities
import java.text.DecimalFormat;
import java.util.Random;


public class Problem1
{

  public static void main(String[] args)
  {
    Random rand = new Random();
    int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10)
               + rand.nextInt(8);
    int num2 = rand.nextInt(655);
    int num3 = rand.nextInt(10000);
    // set formating
    DecimalFormat df3 = new DecimalFormat("###"); 
    DecimalFormat df4 = new DecimalFormat("####"); 
    // define how the system will construct the phonenumber
    String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-"
                         + df4.format(num3);
    // system will display the phone number
    System.out.println(phoneNumber);
  }
}
