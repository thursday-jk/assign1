
//************************************************
// File:        Problem2
// Author:      Kevin Jones
// Date:        23 September 2017
// Course:      CPS 100
//
// PROBLEM STATEMENT: Write a program that reads the radius of a sphere
// and prints its volume and surface area. Use the following formulas.
// Print the output to four decimal places. r represents the radius.
// Volume = V=4/3πr3
// Surface Area = 4 V=4πr2
//
//Inputs: radius of sphere
//Outputs: surface area and volume.
// 
//************************************************
import java.text.DecimalFormat;
import java.util.Scanner;


public class Problem2
{


  public static void main(String[] args)
  {
    // Ask user input for the spheres radius
    System.out.print("Enter The Spheres radius:");
    // declare the scanner object
    Scanner scan = new Scanner(System.in);
    double volume = 0;
    double area = 0;
    double radius = 0;
    DecimalFormat df = new DecimalFormat("0.####");

    radius = scan.nextDouble();
    // close the scanner object
    scan.close();
    // where the system does the calculations
    volume = (4.0 / 3) * Math.PI * radius * radius * radius;
    area = 4 * Math.PI * radius * radius;
    // how the results will be formated
    String volumedf = df.format(volume);
    String areadf = df.format(area);
    // the system displays the results
    System.out.println("The Volume is: " + volumedf);
    System.out.println("The Area is: " + areadf);

  }
}

